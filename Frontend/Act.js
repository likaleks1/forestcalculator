// открытие файла
function openFile() {
    var input = document.createElement("input");//создание элемента input внутри кода
    input.type = "file";// задаем тип в виде "файл"
    $(input).attr('multiple','true');//добавление атрибута для открытия нескольких файлов
    var container = new Array();//создание массива для хранения инфы из файлов
    var reading = new FileReader();//создание FileReader - которая читает файлы
    input.onchange=function () {//onchange событие происходит после изменения формы
        reading.onload = function (info) {//когда все выбранные файлы загрузились, срабатывает onload и код идет дальше
            var p = info.target.result.toString().replace(/\n/g, "<br>");//чтение текста файла
            container.push(p);//добавление элемента в конец массива
            $(".result").html(container);
        };
     for(i=0; i<this.files.length; i++) {//цикл для чтения всех файлов
         reading.readAsText(this.files[i], "UTF-8");//меняет формат файла на "UTF-8"
     }
    };
    input.click();//нажатие кнопки
    return container;//возвращает массив с накопленными файлами
}
//сохранение файла
function saveFile() {
    var text = StringConversion (template);
    var filename = $("#fio").val();
    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
    saveAs(blob, filename+".json");
}


// преобразование JS объекта в JSON строку
function StringConversion(object) {
    var string = JSON.stringify(object);
    return string;

}

// преобразование JSON строки в объект JS
function ObjectСonversion(string) {
    var object = JSON.parse(string);
    return object;
}

//отображение итога для типа деревьев по которому нажимали
$(document).ready(function () {
    $("li[wood-type]").on("click", function () {
        $("div[wood-type-total]").hide();
        $("div[wood-type-total=" + $(this).attr("wood-type") + "]").show();
        $("div[wood-type-data]").hide();
        $("div[wood-type-data=" + $(this).attr("wood-type") + "]").show()

    })
});


$(document).ready(function () {
    var wood = ["spruce", "birch", "aspen", "pine"];
    for(var m=0; m < template.Forest.length; m++) {// Переключение между div
        $("#CreationDiv").append("<div class='table' wood-type-data=" + wood[m] + "></div>");// Создание div
        for (var i = 0; i < template.Forest[m].Woods.length; i++) {// Цикл для добавления внутренностей div
            var h = template.Forest[m].Woods[i].Diametr;
            $("div[wood-type-data=" + wood[m] + "]").append("<div><span class='Diaveter'>" + h + "</span><input class='input' type='number'></div>");
        }
    }
});






