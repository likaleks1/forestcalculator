﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ForestCalculatorBackend
{
	class Program
	{
		static void Main(string[] args)
		{
			var token = new CancellationToken();
			var server = new Server(token,1705);

			var task = new Task(server.Listen);
			task.Start();
			Console.ReadLine();
			Console.WriteLine("Server will be stoped after 60 seconds...");
			

		}
	}
}
