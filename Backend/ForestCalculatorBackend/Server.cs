﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;

namespace ForestCalculatorBackend
{
	public  class Server 
	{
		protected string _outputLine;
		protected int port;
		protected HttpListener httpListener;

		public Server(CancellationToken token, int port)
		{
			this.port = port;
			
		}

		public void Listen()
		{
			httpListener = new HttpListener();
			httpListener.Prefixes.Add($"http://127.0.0.1:{port}/");
			httpListener.Start();
			while (httpListener.IsListening)
			{
				try
				{
					var context = httpListener.GetContext();
					var rawUrl = context.Request.RawUrl;

					var methodName = rawUrl;
					var method = this.GetType().GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
						.Where(x => x.ReturnType == typeof(string))
						.FirstOrDefault(x => methodName.Contains(x.Name));

					if (method == null) continue;
					var inputData = string.Empty;
					var outputData = string.Empty;

					inputData = GetRequestBody(context);
					outputData = (string)method.Invoke(this, new[] { inputData });
					SetResponseBody(context, outputData);
					if (outputData == null) httpListener.Stop(); ;
				}
				catch (Exception ex)
				{

				}
			}
		}

		protected void SetResponseBody(HttpListenerContext context, string response)
		{
			using (var stream = new StreamWriter(context.Response.OutputStream))
			{
				stream.Write(response);
			}
		}

		protected string GetRequestBody(HttpListenerContext context)
		{
			using (var inputStream = new StreamReader(context.Request.InputStream))
			{
				return inputStream.ReadToEnd();
			}
		}

		protected string Ping(string inputData)
		{
			return string.Empty;
		}

		protected string GetAnswer(string inputData)
		{
			return string.Empty;
		}

		protected string PostInputData(string inputData)
		{
			//DoSomething here
			return string.Empty;
		}

		protected string Stop(string inputData)
		{
			return null;
		}

		public virtual void Dispose()
		{
			httpListener.Stop();
		}
	}
}